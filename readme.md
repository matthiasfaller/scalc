#sCalc

s(sport)Calc ist ein Rechner für Läufer und Radfahrer. Er erlaubt die Berechnung von: Distanz, Zeit, Geschwindigkeit und Pace.

Für Distanzen können 5 Presets gespeichert werden.

Fertig unter: [scalc.petrischale.net](http://scalct.petrischale.net)

![Screenshot](http://scalc.petrischale.net/img/screenshot.jpg "Screenshot")

#Hilfe

##Eingaben Berechnungen

| Eingaben                     | Berechnung            |
|------------------------------|-----------------------|
| **Distanz**, Geschwindigkeit | Zeit, Pace            |
| **Zeit**, Distanz            | Geschwindigkeit, Pace |
| **Geschwindigkeit**, Distanz | Zeit, Pace            |
| **Pace**, Distanz            | Zeit, Geschwindigkeit |

Die eingegebenen Daten werden bis zum nächsten Aufruf gespeichert

##Presets
Für die Geschwindigkeit können in der Sektion "Presets" 5 Werte konfiguiert werden. Die Werten werden schon während der Eingabe gespeichert.

Im Rechner können die Werte mit den Buttons automatisch in das Distanzfelder eingetragen werden.

##Icon auf dem Homescreen

Mit folgenden Schritten kann die App als Icon auf dem Homescreen gespeichert werden.

![Schritt 1](http://scalc.petrischale.net/img/Foto_1.jpg "Schritt 1")   
![Schritt 2](http://scalc.petrischale.net/img/Foto_2.jpg "Schritt 2")   
![Schritt 3](http://scalc.petrischale.net/img/Foto_3.jpg "Schritt 3")

#Credits

Erstellt mit Javascript.

- Frameworks: [Pure](http://purecss.io/), [jQuery](https://jquery.com/)
- Glyphicons: [Font Awesome](https://fortawesome.github.io/Font-Awesome/) 
- Appicon: [Ivan Boyko](https://www.iconfinder.com/visualpharm)