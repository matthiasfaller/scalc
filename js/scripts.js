function myRound(zahl,n){
    var faktor = Math.pow(10,n);
    return(Math.round(zahl * faktor) / faktor);
}

function store_local(key, data) {
    /*
    Saves date object to local storage
    */
    localStorage.setItem(key, JSON.stringify(data));
}

function read_local(key) {
    /*
    Reads data object from local storage

    Returns: Object
    */
    return JSON.parse(localStorage.getItem(key))
}

function pace_calc() {
    this.read_form = function() {
        /*
        Read form values into object properties
        */
        this.time_h = $("#time_h").val();
        this.time_m = $("#time_m").val();
        this.time_s = $("#time_s").val();
        this.distance = $("#distance").val();
        this.speed = $("#speed").val();
        this.pace_m = $("#pace_m").val();
        this.pace_s =$("#pace_s").val();
    };

    this.read_json = function(data) {
        /*
        Reads json object 
        */
        this.time_h = data.time_h;
        this.time_m = data.time_m;
        this.time_s = data.time_s;
        this.distance = data.distance;
        this.speed = data.speed;
        this.pace_m = data.pace_m;
        this.pace_s =data.pace_s;
    }

    this.set_time = function() {
        /*
        Calculates time from speed and distance
        */
        var t =  this.distance/(this.speed/3600);

        this.time_h = Math.floor(t / 3600);
        t -= this.time_h * 3600;
        this.time_m = Math.floor(t / 60);
        t -= this.time_m * 60;
        this.time_s = parseInt(t); //Nachkomma abschneiden
    };

    this.set_pace = function() {
        /*
        Calculates pace from speed
        */
        var t = 3600 / this.speed

        this.pace_m = Math.floor(t / 60);
        t -= this.pace_m * 60
        this.pace_s = parseInt(t); //Nachkomma abschneiden
    };

    this.set_speed_from_pace = function() {
        /*
        Calculates speed from pace
        */
        var pace = this.get_pace();
        this.speed = 3600 / pace;
    };

    this.set_speed = function() {
        /*
        Calculates speed from time and distance
        */
        var time = this.get_time();
        this.speed = this.distance/(time / 3600);
    }

    this.display_data = function() {
        /*
        Writes data object to the form fields. Property name indicated field name,
        property value will be written to field value.
        */
        $("#time_h").val(this.time_h);
        $("#time_m").val(this.time_m);
        $("#time_s").val(this.time_s);
        $("#distance").val(myRound(this.distance, 1));
        $("#speed").val(myRound(this.speed, 1));
        $("#pace_m").val(this.pace_m);
        $("#pace_s").val(this.pace_s);
    };

    this.get_time = function() {
        /*
        Returns time in seconds
        */
        return this.time_h * 3600 + this.time_m * 60 + this.time_s * 1;
    }

    this.get_pace = function() {
        /*
        Returns pace in seconds
        */
        return this.pace_m * 60 + this.pace_s * 1;
    };
}


function presets_data() {
    this.read_form = function() {
        /*
        Reads preset form
        */
        var val = [];
        $("#preset_form input").each(function() {
           val.push($(this).val());
            
        });
        this.presets = val;
    }

    this.read_json = function(data) {
        /*
        Reads json object 
        */
        this.presets = data.presets;
    }

    this.display_data = function() {
        /*
        Displays data in form
        */
        for (var i = 0; i < this.presets.length; i++) {          
            $("#preset_" + i).val(this.presets[i]);
        };
    }
}

function init() {
    /*
    Init page
    */
    var local_data = read_local("data");
    if (local_data) {
        var data = new pace_calc();
        data.read_json(local_data);
        data.display_data();
    };
    var local_presets = read_local("presets")
    if (local_presets) {
        var presets = new presets_data();
        presets.read_json(local_presets);
        presets.display_data();

        generate_preset_button(presets);
    }
}

function distance_handler() {
    /*
    Handler for changes in distance field
    */
    var data = new pace_calc();
    data.read_form();
    data.set_time();

    data.display_data();
    store_local("data", data);
}

function speed_handler() {
    /*
    Handler for changes in speed field
    */
    var data = new pace_calc();
    data.read_form();
    data.set_time();
    data.set_pace();

    data.display_data();
    store_local("data", data);
}

function time_handler() {
    /*
    Handler for changes in time fileds
    */
    var data = new pace_calc();
    data.read_form();
    data.set_speed();
    data.set_pace();

    data.display_data();
    store_local("data", data);
}

function pace_handler() {
    /*
    Handler for changes in pace fields
    */
    var data = new pace_calc();
    data.read_form();
    data.set_speed_from_pace();
    data.set_time();
    
    data.display_data();
    store_local("data", data);
}

function presets_handler() {
    /*
    Handler for changes in preset field
    */
    var presets = new presets_data();
    presets.read_form();

    store_local("presets", presets);
    
    generate_preset_button(presets);
}

function generate_preset_button(data) {
    /*
    Generates preset buttons
    */
    if ($("#input_form").find("#preset_buttons").length > 0) {
        $("#preset_buttons").find(".preset_button").remove();       
    } else {
        $("#distance").after('<div id="preset_buttons"></div>');
    }   
  
    for (var i = 0; i < 5; i++) {
        var val = data.presets[i];
        var html = '<a class="pure-button preset_button" href="#">' + val + '</a>'
        $("#preset_buttons").append(html)            
    };

    $('.preset_button').on('click', function(event) {
        event.preventDefault();
        $("#distance").val($(this).text());
        distance_handler();
    });
}